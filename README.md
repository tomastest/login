# Full-Stack Developer Candidate Task

## About

This task is meant to evaluate candidates frontend (HTML, CSS, JS) and backend (PHP) knowledge.

## Instructions

- Design file can be found at `assets/login.psd` in this repository;
- Show us what you got 💪
- Create a merge request and let us know

## Requirements

- Design should look as close to the given design, as possible;
- Design must be responsive;
- App should work modern browsers & IE11+;
- Encouraged to use pre-processors for CSS, but that's optional;
- Email & password validation (not empty, valid email format);
- Create an api endpoint to login the user (only valid credentials of email: `imfullstack@imawesome.com`, password: `cool`);
- Throw a greeting message if login was successful.

## Tips

- Using framework is ok, but if you have the time & skill, show us your pure solution;
- We love clean code with good structure!
