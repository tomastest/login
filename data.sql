CREATE DATABASE  IF NOT EXISTS `imawesome`;
USE `imawesome`;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


INSERT INTO `users` VALUES (1,'imfullstack@imawesome.com','$2y$10$NzzhrbGMc4dqmcaEHSGWdO2mgHQ5uWMmhbWDKMjO9/AQaw/6vvqpa');

