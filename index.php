<?php
require "db.php";
require "Validator.php";
if (!empty($_POST)) {
    $username = $_POST['username'] ?? null;
    $password = $_POST['password'] ?? null;

    $error = Validator::validate($username, $password);

    if (empty($error)) {
        $_SESSION['user'] = Validator::returnUserId($username);
        header('Location: greetings.php');
        exit;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
body{
    padding-top: 40px;
    background-color: #451549;
        }
    </style>
</head>
<body>
    <div class="container">
        <?php if (!empty($error)) :?>
            <div class="p-3 mb-2 bg-danger text-white"><?php echo $error;?></div>
        <?php endif;?>
        <div class="row justify-content-md-center">
            <div class="col-md-4 col-md-offset-2">
                <div class="card card-default">
                    <div class="card-header text-white" style="background-color: #755c7e;">
                        Login Form
                    </div>
                    <div class="card-body">
                        <form action="index.php" method="post">
                            <div class="form-group">
                                <input type="text" name="username" placeholder="Username" class="form-control" value="<?php echo $username??'';?>">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" style="background-color: #755c7e;">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>