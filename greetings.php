<?php
require "db.php";
if (!isset($_SESSION['user'])) {
    header('Location: index.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Greetings</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body{
            padding-top: 40px;
            background-color: #451549;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="p-3 mb-2 bg-success text-white">Welcome user: <?php echo $_SESSION['user'];?></div>
    <div class="p-3 mb-2 bg-info text-white">
        <a href="logout.php">Logout</a>
    </div>
</div>
</body>
</html>