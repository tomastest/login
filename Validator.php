<?php

class Validator
{
    public static function validate($username, $pass)
    {
        if (self::isFieldEmpty($username)) {
            return "Username is empty.";
        }

        if (!self::isEmailValid($username)) {
            return "Email is not valid.";
        }

        if (self::isFieldEmpty($pass)) {
            return "Password is empty.";
        }

        if (!self::isEmailRegistered($username)) {
            return "Email does not exist.";
        }

        if (!self::checkIfPasswordCorrect($username, $pass)) {
            return "password is incorrect.";
        }
    }

    public static function isFieldEmpty($field)
    {
        return empty($field);
    }

    public static function isEmailValid($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function isEmailRegistered($email)
    {
        return (bool) R::findOne('users', ' email = ?', [$email]);
    }

    public static function checkIfPasswordCorrect($email, $pass)
    {
        $user = R::findOne('users', ' email = ?', [$email]);
        if ($user) {
            return password_verify($pass, $user->password);
        } else {
            return false;
        }
    }
    public static function returnUserId($email)
    {
        $user = R::findOne('users', ' email = ?', [$email]);
        return $user->id;
    }
}
